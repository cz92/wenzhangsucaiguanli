<?php

/**
 * 公用的方法
 */

function  show($status, $message,$data=array()) {
    $reuslt = array(
        'status' => $status,
        'message' => $message,
        'data' => $data,
    );
    exit(json_encode($reuslt));
}
function getMd5Password($password) {
    return md5($password . C('MD5_PRE'));
}
function showKind($status,$data) {
    header('Content-type:application/json;charset=UTF-8');
    if($status==0) {
        exit(json_encode(array('error'=>0,'url'=>$data)));
    }
    exit(json_encode(array('error'=>1,'message'=>'上传失败')));
}
function check_verify($code, $id = ''){
    $verify = new \Think\Verify();
    return $verify->check($code, $id);
}
function md5Token($username,$pwd,$timeout = null){
    $time = time();
    $md5pre = C('MD5_PRE');
    $token  = md5($username.$pwd.$time.$md5pre);
    setcookie( 'token' , $token , $timeout );
    return $token;
}
function object_array($array)
{
    if (is_object($array)) {
        $array = (array)$array;
    }
    if (is_array($array)) {
        foreach ($array as $key => $value) {
            $array[$key] = object_array($value);
        }
    }
    return $array;
}
function sendStatus($id){
    if($id == 1){
        return "草稿";
    }else if($id == 2){
        return "已发布";
    }else if($id == -1){
        return "已删除";
    }else{
        return "";
    }
}



