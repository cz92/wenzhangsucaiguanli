<?php
namespace Common\Model;
use Think\Model;

class AdminModel extends Model {
	private $_db = '';

	public function __construct() {
		$this->_db = M('admin');
	}
   
    public function getAdminByUsername($username='') {
        $res = $this->_db->where('username="'.$username.'"')->find();
        return $res;
    }
    public function getAdminByAdminId($adminId=0) {
        $res = $this->_db->where('admin_id='.$adminId)->find();
        return $res;
    }
    public function insertToken($user="",$token = ""){
        if(empty($token) || empty($user)){
            return ;
        }
        $data['token'] = $token;
        $res = $this->_db->where("username = '".$user ."'")->save($data);
        if(!$res){
            throw_exception("token更新失败");
        }
        return $res;
    }
    public function getUserForToken($token = ""){
        $res = $this->_db->where('token="'.$token.'"')->find();
        return $res;
    }
}
