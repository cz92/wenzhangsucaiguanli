<?php
/**
 * Created by PhpStorm.
 * User: cz
 * Date: 2017/3/30
 * Time: 15:18
 */
 namespace Common\Model;
 use Think\Exception;
 use Think\Model;
 class NewsModel extends Model{
     public function contentForm($arr = array()){
         $Article = M("news");
         $Article->startTrans();
         if (!$Article->autoCheckToken($arr)){
             return show(0,'兄弟阿,别再提交了哦',"");
         }else{
             $data['title'] = $arr['articletitle'];
             $data['keywords'] = $arr['keywords'];
             $data['description'] = $arr['abstract'];
             $data['thumb'] = $arr['thumb'];
             $data['username'] = session("cz.username");
             $data['author'] = session("cz.username");
             $rootStr = dirname(__FILE__);
             $imgUrl = explode(__ROOT__,$rootStr)[0].__ROOT__.$arr['thumb'];
             $weixinMedia = $this->getMediaId(session('we_token'),$imgUrl);
             $weixinJson = json_decode($weixinMedia);
             $returnWei = object_array($weixinJson);
             if(!$returnWei['media_id']){
                 return show(0,'提交失败了阿',"");
             }
             $data['media_id'] = $returnWei['media_id'];
             $data['media_url'] = $returnWei['url'];
             $resId = $Article->add($data);
             if($resId){
                 $Content = M("content");
                 $articleContent['article_content'] = htmlspecialchars($arr['editorValue']);
                 $articleContent['article_id'] = $resId;
                 $cId = $Content->add($articleContent);
                 if($resId && $cId){
                     $Article->commit();
                     return show(1,'提交成功了哦',"");
                 }else{
                     $Article->rollback();
                     return show(1,'提交失败了',"");
                 }
             }
         }
     }
     public function getMediaId($token,$file){
         $url = "https://api.weixin.qq.com/cgi-bin/material/add_material?access_token=".$token;
         $filedata = array('media' => new \CURLFile($file));
         $ch = curl_init($url);
         curl_setopt ( $ch, CURLOPT_SAFE_UPLOAD, true);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch, CURLOPT_POST, 1);
         curl_setopt($ch, CURLOPT_POSTFIELDS, $filedata);
         $result = curl_exec($ch);
         return $result;
     }
     public function getNews(){
         $articleList = M("news");
         $res = $articleList->where("status != -1")->select();
         return $res;
     }
     public function delArticle($delarr = array()){
         if(empty($delarr)){
             exit;
         }
         try{
             $Article = M("news");
             $str = "id in (";
             $data["status"] = -1;
             for($i = 0; $i < count($delarr); $i++){
                 if($i + 1 == count($delarr)) {
                     $str.=$delarr[$i].")";
                 }else{
                     $str.=$delarr[$i].",";
                 }
             }
             //print_r($str);die();
             $Article->where($str)->save($data);
             //print_r($Article->_sql());die();
             return show(1,'删除成功了哦',"");
         }catch(Exception $e){
             throw_exception("删除失败");
         }
     }
 }