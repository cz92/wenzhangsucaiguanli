<?php
return array(
	//'配置项'=>'配置值'
    'MULTI_MODULE'          =>  true,
    'SESSION_AUTO_START'    => true,
    'URL_MODEL'=>0,
    'DATA_CACHE_TYPE' => 'Memcache',
    'MEMCACHE_HOST'   => 'localhost',
    "MEMCACHE_PORT"   => '11211',
    'DATA_CACHE_TIME' => 0,
    'DB_TYPE'   => 'mysql', // 数据库类型
    'DB_HOST'   => '182.254.146.35', // 服务器地址
    'DB_NAME'   => 'rulaizhang_weixin_article', // 数据库名
    'DB_USER'   => 'root', // 用户名
    'DB_PWD'    => 'czrootcz', // 密码
    'DB_PORT'   => 3306, // 端口
    'DB_PREFIX' => 'rulaizhang_', // 数据库表前缀
    'DB_CHARSET'=> 'utf8', // 字符集
    'DB_DEBUG'  =>  TRUE, // 数据库调试模式 开启后可以记录SQL日志 3.2.3新增
    'DEFAULT_CHARSET'       =>  'utf-8', // 默认输出编码
    'DEFAULT_TIMEZONE'      =>  'PRC',  // 默认时区
    'DEFAULT_AJAX_RETURN'   =>  'JSON',  // 默认AJAX 数据返回格式,可选JSON XML ...
    'DEFAULT_JSONP_HANDLER' =>  'jsonpReturn', // 默认JSONP格式返回的处理方法
    'DEFAULT_FILTER'        =>  'htmlspecialchars', // 默认参数过滤方法 用于I函数...
    'DEFAULT_LANG'          =>  'zh-cn', // 默认语言
    'TOKEN_ON'      =>    true,  // 是否开启令牌验证 默认关闭
    'TOKEN_NAME'    =>    '__hash__',    // 令牌验证的表单隐藏字段名称，默认为__hash__
    'TOKEN_TYPE'    =>    'md5',  //令牌哈希验证规则 默认为MD5
    'TOKEN_RESET'   =>    true,  //令牌验证出错后是否重置令牌 默认为true
    'MD5_PRE' => 'rulaizhang',
);