<?php
/**
 * Created by PhpStorm.
 * User: cz
 * Date: 2017/3/29
 * Time: 13:26
 */
 namespace Admin\Controller;
 use Think\Controller;
 use Think\Exception;

 class ArticleListController extends CommonController{
    public function index(){
        $result = D("News")->getNews();
        $userCount = D("News")->count();
        $this->assign("total" ,$userCount );
        $this->assign("newsList",$result);
        $this->display();
    }
     public function delArr(){
        $delArr = $_POST["delId"];
        if(!empty($delArr)) {
            try{
                D("News")->delArticle($delArr);
            }catch(Exception $e){
                $this->error($e->getMessage(),true);
            }

        }
     }
 }