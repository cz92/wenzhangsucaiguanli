<?php
namespace Admin\Controller;
use Think\Controller;

class CommonController extends Controller {


	public function __construct() {
		
		parent::__construct();
		$this->_init();
	}

	private function _init() {
		// 如果已经登录
		$isLogin = $this->isLogin();
		if(!$isLogin) {
			// 跳转到登录页面
			header("Content-type: text/html; charset=" . C('DEFAULT_CHARSET'));
			$this->redirect('/admin.php?c=login');
		}
	}
	public function getLoginUser() {
		if(cookie("token") && empty(session("cz"))){
			$userRes = D("Admin")->getUserForToken(cookie('token'));
			session("cz",$userRes);
		}
		return session("cz");
	}

	public function isLogin() {
		$user = $this->getLoginUser();
		if(!empty($user)) {
			return true;
		}

		return false;
	}

}