<?php
/**
 * Created by PhpStorm.
 * User: cz
 * Date: 2017/3/29
 * Time: 10:43
 */
 namespace Admin\Controller;
 use Think\Controller;
 use Think\Exception;
 class LoginController extends Controller{
     public function index(){
         if(cookie('token')){
             $userRes = D("Admin")->getUserForToken(cookie('token'));
             if($userRes){
                 $username = $userRes['username'];
                 $password = $userRes['password'];
                 $dataToken = $userRes['token'];
                 if(cookie('token') == $dataToken){
                     $token = md5Token($username,$password);
                     try{
                         $tokenResult = D("Admin")->insertToken($username,$token);
                         if(!$tokenResult){
                             $this->error("亲，服务器正忙，请重新登录哦！" , "" ,true);
                         }
                     }catch(Exception $e){
                         $this->error($e->getMessage().",请重新登录",true);
                     }
                     session("cz",$userRes);
                     header("Content-type: text/html; charset=" . C('DEFAULT_CHARSET'));
                     $this->redirect("/admin.php?c=index");
                 }
             }
         }
         $this->display();
     }
     public function verify_c(){
         $Verify = new \Think\Verify();
         $Verify->fontSize = 18;
         $Verify->length   = 4;
         $Verify->useZh = true;
         $Verify->useImgBg = true;
         $Verify->zhSet = '们以我到他会作时要动日出生于台湾省新北市中国台湾流行乐男歌手音乐人演员导演编剧监制国产的一是工就年阶义发成部民可出能方进在了不和有大这爱程柱上海';
         //$Verify->expire = 600;
         $Verify->entry();
     }
/*     public function acceptToken(){
         $loginToken = I('post.token','');
         if($loginToken == cookie('token')){
             $userRes = D("Admin")->getAdminByUsername(cookie('token'));
             session("cz",$userRes);
             header("Content-type: text/html; charset=" . C('DEFAULT_CHARSET'));
             $this->redirect("admin.php?c=index&a=index");
         }
     }*/
     public function loginUser(){
         $successUrl = "admin.php?c=index&a=index";
         $username = I('post.username','');
         if(empty($username)){
             $this->error("亲，没有用户名！","",true);
         }
         $password = I('post.password','');
         if(empty($password)){
             $this->error("亲，没有密码！","",true);
         }
         $userRes = D("Admin")->getAdminByUsername($username);
         if(!$userRes || $userRes['status'] != 1){
             $this->error("亲，用户名输入错误","",true);
         }
         if($userRes['password'] != getMd5Password($password)) {
             $this->error("亲，密码输入错误","",true);
         }
         $verify = I('post.verify','');
         if(!check_verify($verify)){
             $this->error("亲，验证码输错了哦！","",true);
         }
         $onlie = isset($_POST['online']) ? $_POST['online'] : null;
         $token = "";
         if(!is_null($onlie)){
             $token = md5Token($username,$password,time()+60*60*24*7);
             try{
                 $tokenResult = D("Admin")->insertToken($username,$token);
                 if(!$tokenResult){
                     $this->error("亲，服务器正忙，请重新登录哦！" , "" ,true);
                 }
             }catch(Exception $e){
                 $this->error($e->getMessage().",请重新登录",true);
             }
         }
         $returnData = array(
             "info"=> "亲，登录成功!",
             "url"    =>  $successUrl,
             "status" =>  1,
             "token"  =>  $token
         );
         session('cz', $userRes);
         $this->ajaxReturn($returnData);
    }
     public function loginout() {
         session('cz',null);
         setcookie("token","");
         $this->redirect('/admin.php?c=login&a=index');
     }
 }