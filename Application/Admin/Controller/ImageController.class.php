<?php
/**
 * Created by PhpStorm.
 * User: cz
 * Date: 2017/3/30
 * Time: 10:44
 */
 namespace Admin\Controller;
 use Think\Controller;
 use Think\Upload;
 use Com\Wechat;
 use Com\WechatAuth;
 class ImageController extends CommonController{
     private  $appid="wx7a40b9a086f0bd81";
     private  $appSecret="d29d30a9709956940dd1c8146407ffaa";
     private  $WechatAuth="";//初始化WechatAuth类
     private  $accsess_token="";//缓存token
     public function addImage(){
         $upload = D("UploadImage");
         $res = $upload->imageUpload();
         if($res===false) {
             return show(0,'上传失败','');
         }else{
             return show(1,'上传成功',$res);
         }
     }
     public function subMitContent(){
         //print_r($_POST);die();
         //session("aaaa","1111111111");
         if($_POST){
             if(!isset($_POST['articletitle']) || !$_POST['articletitle']) {
                 return show(0,'标题不存在');
             }
             if(!isset($_POST['keywords']) || !$_POST['keywords']) {
                 return show(0,'搜索关键字不存在');
             }
             if(!isset($_POST['abstract']) || !$_POST['abstract']) {
                 return show(0,'文章摘要部分');
             }
             if(!isset($_POST['thumb']) || !$_POST['thumb']) {
                 return show(0,'没有头图');
             }

             if(!isset($_POST['editorValue']) || !$_POST['editorValue']) {
                 return show(0,'没有文章');
             }
             if(!session('we_token') || session("expire") < time()){
                 $this->WechatAuth=new WechatAuth($this->appid,$this->appSecret);//初始化WechatAuth类
                 $WechatAuth=$this->WechatAuth;
                 $token=$WechatAuth->getAccessToken();
                 session(array('expire'=>time()+$token['expires_in']));//设置过期时间
                 session('we_token',$token['access_token']);//缓存token
                 //print_r(session('we_token'));
                 //print_r(session("aaaa"));
                 $this->accsess_token=$token;
             }else{
                 $token=session('we_token');
                 $this->WechatAuth=new WechatAuth($this->appid,$this->appSecret,$token);//初始化WechatAuth类
                 $this->accsess_token=$token;//缓存token
             }
             $res=D('News')->contentForm($_POST);
         }
     }
 }